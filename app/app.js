'use strict';

/**
 * @ngdoc module
 * @name app
 * @description
 * The GTD application.
 *
 * Everything goes there.
 */
angular.module('app', [
  'ngRoute'
  // TODO Ajouter si nécessaire
])
  .config([
    '$routeProvider',
    function($routeProvider) {
      $routeProvider.otherwise({
        templateUrl: '404.html'
      });
    }
  ])
;
