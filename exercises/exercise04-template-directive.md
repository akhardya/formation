# Exercice 4 : Utiliser une directive pour mutualiser du code (démarrer de la branche `exercice03`)

Nous allons mutualiser le code des listes de *home/home.html* afin qu'il soit plus lisible

## Design d'API (discussion en groupe)

Contraintes:
- Modèle de données: il faut faire attention !
- Les panels peuvent avoir un style différent
- On doit limiter la taille des panels, et effectuer une action lors d'un click sur le dernier lien
    - On peut faire sauter la limite
    - On peut laisser le choix à l'utilisateur
- Quelle liberté laisse-t-on pour le titre du panel ? 
    - Juste un texte ?
    - On pourrait avoir du HTML ? Bindé ?
- Directive sur élément vs. directive sur attribut ? Quid des directives sur classe ?
- Performance

## [Écriture de la directive](https://code.angularjs.org/1.4.7/docs/guide/directive)

1. Initialiser la directive `gtdTasksPreview` dans *common/tasksPreviewDirective.js*
1. Initialiser le template (il s'agit de *common/tasksPreviewDirective.html*)
1. Prendre les paramètres. Le seul qui soit imposé aujourd'hui dans le template est `tasks`. Comment le binder ?
1. Binder le style de panel
1. Binder la limite et l'action pour afficher tout


On a maintenant un code bien mutualisé, on peut donner à chaque tâche son URL.
